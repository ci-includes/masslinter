# Masslinter, a GitLab pipeline linter collection
As GitLab does not have a [marketplace][marketplace] of sorts for linters,
using a dedicated repo with a few includes is the 'next best thing'.


## Linters
The following section describes the available linters. All linters write their
output to a directory called `reports`.

A special linter exists, `all-linters.yml` that just aggregates all linters.
Including this file tries to run all linters available.

Linters are typically not run when the `run pipeline` button is pushed causing
the pipeline source to be `web`, but do offer the user the choice to manually
run the linter, if the required configuration file is present.

Any linter will only ever run on the main git repository. Git submodules are
excluded.

Including the linter(s) follows the [GitLab includes][gitlab_includes] way:

```yaml
include:
  - project: 'ci-includes/masslinter'
    ref: 'master'
    file: 'all-linters.yml'

```

> _Note:_ To offer more flexibility when including individual linters, they
> are hidden jobs by default, and require to be extended.

For example, to explicitly include the gitcheck linter:

```yaml
include:
  - project: 'ci-includes/masslinter'
    ref: 'master'
    file:
      - 'common.yml'
      - 'gitcheck.yml'
masslinter_gitcheck:
  extends: .masslinter_gitcheck
```


### Gitcheck
A simple check ensures the commit history in a merge request is clean.
Cleanliness is determined by `git log --check` only.

To allow failure of the linter create an (empty) file called
`.gitcheck_allow_failure`.

This linter can only be triggered on branches that have a merge requests, as
it needs the CI variable `$CI_MERGE_REQUEST_TARGET_BRANCH_NAME` to figure out
against what to run the check.


### Hadolint
The Dockerfile linter [hadolint][hadolint] is used to validate both
`Containerfile`s and `Dockerfile`s in any sub directory.

To trigger hadolint, either a hadolint configuration file is needed called
`.hadolint.yaml`, `.hadolint.yml` _or_ `.hadolint_allow_failure`, where the
later is used to allow failures from the pipeline on hadolinting errors. The
hadolint config file is still honored in both cases.


### Markdownlint
The markdown linter [markdownlint][markdownlint] is used to validate any
markdown scripts.

To trigger the markdown pipeline, a `.mdlrc` file _or_
`.markdownlint_allow_failures`, where the later is used to allow failures from
the pipeline on markdownlint errors, needs to be present. The markdownlint
config file is still honored in both cases.


### Shellcheck
The shell script linter [shellcheck][shellcheck] is used to validate any shell
scripts.

To trigger the shellcheck pipeline, a `.shellcheckrc` file _or_
`.shellcheck_allow_failures`, where the later is used to allow failures from
the pipline on shellcheck errors, needs to be present. The shellcheck config
file is still honored in both cases.

Because shell scripts can have any name, by default, only files with the
extension `.sh` are checked. To add more extensions to be checked, the variable
`MASSLINTER_SHELLCHECK_EXT` can be filled with additional extensions. This
variable can be separated with `$IFS` values such as space or new lines. An
extension should not include the preceding period (`.`).

```sh
sh bash
zsh ksh
```

In addition to the above, files without extensions can be explicitly listed
using `MASSLINTER_SHELLCHECK_PATHS`. Note, that this is basically an input to
`find -wholename` to match filenames with paths in them. Paths that contain
spaces are not really supported, but should work if properly escaped.

```sh
bin/somescript
shellscripts
libexec/otherscript
```

In the above example, all scripts called `shellscripts` would be found, in any
directory, as well as the other two scripts, as long as it matches the prefix.
So `usr/bin/somescript` would also match.


## Contributing
Contributions are done using Pull Requests. Note that ironically this repo
won't have any linters probably itself, but the shell code within the yaml
file is expected to be posix/shellcheck compliant.

Each linter is to be its individual yaml file for ease inclusion. The filename
shall reflect the name of the linter, followed by the `.yml` extension as is
common with GitLab. The linter can be added, in alphabetical order, to the
special file called `all-linters.yml`.

This is a somewhat different concept as to how GitHub's megalinter works, where
a single container is responsible for all linters. One of the design reasons
against this, is to avoid having to maintain said container. Further more, it
is also much harder to run parallel tasks.


[gitlab_includes]: https://docs.gitlab.com/ee/ci/yaml/includes.html
[hadolint]: https://github.com/hadolint/hadolint
[marketplace]: https://gitlab.com/gitlab-org/gitlab/-/issues/276004
[markdownlint]: https://github.com/markdownlint/markdownlint
[shellcheck]: https://shellcheck.net
