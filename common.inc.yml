# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2021 Olliver Schinagl <oliver@schinagl.nl>

.checkstyle_convert:
  - xmlstarlet --version
  - |
    cat > "${TMP:=/tmp}/checkstyle2junit.xslt" <<'EOT'
    <?xml version="1.0" encoding="UTF-8"?>
    <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
      <xsl:output encoding="UTF-8" method="xml"></xsl:output>

      <xsl:template match="/">
        <testsuite>
          <xsl:attribute name="tests">
            <xsl:value-of select="count(.//file)" />
          </xsl:attribute>
          <xsl:attribute name="failures">
            <xsl:value-of select="count(.//error)" />
          </xsl:attribute>
          <xsl:for-each select="//checkstyle">
            <xsl:apply-templates />
          </xsl:for-each>
        </testsuite>
      </xsl:template>

      <xsl:template match="file">
        <testcase>
          <xsl:attribute name="classname">
            <xsl:value-of select="@name" />
          </xsl:attribute>
          <xsl:attribute name="name">
            <xsl:value-of select="@name" />
          </xsl:attribute>
          <xsl:apply-templates select="node()" />
        </testcase>
      </xsl:template>

      <xsl:template match="error">
        <failure>
          <xsl:attribute name="type">
            <xsl:value-of select="@source" />
          </xsl:attribute>
          <xsl:text>Line </xsl:text>
          <xsl:value-of select="@line" />
          <xsl:text>&#58; </xsl:text>
          <xsl:value-of select="@message" />

          <xsl:text> See </xsl:text>
          <xsl:choose>
            <xsl:when test="substring(@source, string-length(@source) - 5, 2) = 'SC'">
              <xsl:text>https://www.shellcheck.net/wiki/</xsl:text>
            </xsl:when>
            <xsl:when test="substring(@source, string-length(@source) - 5, 2) = 'DL'">
              <xsl:text>https://github.com/hadolint/hadolint/wiki/</xsl:text>
            </xsl:when>
            <xsl:otherwise/>
          </xsl:choose>
          <xsl:value-of select="substring(@source, string-length(@source) - 5, 6)"/>

        </failure>
      </xsl:template>
    </xsl:stylesheet>
    EOT

    cat > "${TMP:=/tmp}/checkstyle2text.xslt" <<'EOT'
    <?xml version="1.0" encoding="UTF-8"?>
    <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
      <xsl:output encoding="UTF-8" method="text"/>

      <xsl:template match="/">
        <xsl:apply-templates select="//file" />
      </xsl:template>

      <xsl:template match="file">
        <xsl:variable name="filename" select="@name" />
        <xsl:variable name="errorcount" select="count(error)" />

        <xsl:value-of select="$filename" />
        <xsl:text> - </xsl:text>
        <xsl:value-of select="$errorcount" />
        <xsl:text> failure(s)</xsl:text>
        <xsl:text>&#10;</xsl:text>

        <xsl:apply-templates select="error">
          <xsl:with-param name="filename" select="$filename"/>
        </xsl:apply-templates>

        <xsl:text>&#10;&#10;</xsl:text>
      </xsl:template>

      <xsl:template match="error">
        <xsl:param name="filename"/>
        <xsl:value-of select="$filename" />
        <xsl:text>&#58; </xsl:text>
        <xsl:value-of select="@source" />
        <xsl:text> (</xsl:text>
        <xsl:value-of select="@severity" />
        <xsl:text>) line </xsl:text>
        <xsl:value-of select="@line" />
        <xsl:text>, column </xsl:text>
        <xsl:value-of select="@column" />
        <xsl:text> - </xsl:text>
        <xsl:value-of select="@message" />
        <xsl:text>&#10;</xsl:text>
        <xsl:text> - </xsl:text>
        <xsl:value-of select="@message" />
        <xsl:text>&#10;</xsl:text>
      </xsl:template>
    </xsl:stylesheet>
    EOT
  - |
    while IFS='' read -r _file; do
      if [ ! -f "${_file}" ]; then
        continue
      fi
      xmlstarlet tr "${TMP:=/tmp}/checkstyle2junit.xslt" "${_file}" > "${_file%%.checkstyle.xml}.junit.xml"
      xmlstarlet tr "${TMP}/checkstyle2text.xslt" "${_file}" | \
      sed 's|"|\\"|g' | \
      sed -n \
          -e "s|^\(.*\) - \([0-9]\+\) failure.*$|echo '========='\n \
              echo 'Found \2 failure(s) in \1'\n \
              echo '---------'|p" \
          -e "s|^\(.*\): \(ShellCheck\.\)\?\(\(..\)[0-9]\{4\}\) (\(.*\)) line \([0-9]\+\), column \([0-9]\+\) - \(.*\)$|echo 'In \1 line \6:'\n \
              sed -n '\6p' \1\n \
              printf \"%\$((\7 - 1\))s^ \"\n \
              case \4 in \
              DL) \
                printf 'https://github.com/hadolint/hadolint/wiki/' \
                ;; \
              SC) \
                printf 'https://www.shellcheck.net/wiki/' \
                ;; \
              esac\n \
              echo \"\3 (\5): \8\"\n \
              echo\n|p" | \
      sh | \
      tee -a "${_file%%.checkstyle.xml}.log"
    done <<-EOT
    $(eval "find '${CI_PROJECT_DIR}/reports' -type f -iname '*.checkstyle.xml'")
    EOT

.common_script_functions:
  - |
    add_file_ext()
    {
      if [ -z "${1:-}" ]; then
        return
      fi
      for _ext in ${1}; do
        _extra_ext="-o -name '*.${_ext}' ${_extra_ext:-}"
      done
      echo "${_extra_ext:-}"
    }
  - |
    add_path_name()
    {
      if [ -z "${1:-}" ]; then
        return
      fi
      for _path in ${1}; do
        _extra_path="-o -wholename '${_path}' ${_extra_path:-}"
      done
      echo "${_extra_path:-}"
    }
  - |
    submodule_check()
    {
      if [ ! -f "${CI_PROJECT_DIR}/.gitmodules" ]; then
        return
      fi
      while IFS='' read -r _submodule; do
        if [ ! -d "${CI_PROJECT_DIR}/${_submodule:-}" ]; then
          continue
        fi
        _skip_submodules="-not -path '${CI_PROJECT_DIR}/${_submodule}/*' ${_skip_submodules:-}"
      done <<-EOT
    $(sed -n 's|^[[:space:]]*path[[:space:]]*=[[:space:]]*\(.*\)$|\1|p' "${CI_PROJECT_DIR}/.gitmodules")
    EOT
      echo "${_skip_submodules:-}"
    }
